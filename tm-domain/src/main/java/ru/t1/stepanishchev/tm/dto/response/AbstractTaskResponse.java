package ru.t1.stepanishchev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;
import ru.t1.stepanishchev.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

    public AbstractTaskResponse(@Nullable TaskDTO task) {
        this.task = task;
    }

}