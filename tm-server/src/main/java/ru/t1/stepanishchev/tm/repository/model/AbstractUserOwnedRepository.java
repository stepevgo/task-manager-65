package ru.t1.stepanishchev.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

@Repository
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

}