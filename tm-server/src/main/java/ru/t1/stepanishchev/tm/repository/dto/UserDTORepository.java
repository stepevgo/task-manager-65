package ru.t1.stepanishchev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;

@Repository
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findOneById(@NotNull final String id);

    @Nullable
    UserDTO findOneByLogin(@NotNull final String login);

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

}