package ru.t1.stepanishchev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
public interface ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findByUserId(@NotNull String userId);

    @Nullable
    ProjectDTO findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sortType")
    List<ProjectDTO> findAllByUserIdAndSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortType") String sortType);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    long countByUserId(@NotNull final String userId);

}