package ru.t1.stepanischev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.stepanischev.tm.service.ProjectService;
import ru.t1.stepanischev.tm.service.TaskService;

@Controller
public class TasksController {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private ProjectService projectService;

    @GetMapping("/tasks")
    public ModelAndView index() {
        @NotNull ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        modelAndView.addObject("projectRepository", projectService);
        return modelAndView;
    }

}