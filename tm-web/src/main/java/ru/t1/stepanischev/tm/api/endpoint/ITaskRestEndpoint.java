package ru.t1.stepanischev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanischev.tm.model.TaskDTO;

import java.util.Collection;

public interface ITaskRestEndpoint {

    void delete(TaskDTO task);

    void delete(String id);

    @Nullable
    Collection<TaskDTO> findAll();

    @Nullable
    TaskDTO findById(String id);

    @NotNull
    TaskDTO save(TaskDTO task);

}