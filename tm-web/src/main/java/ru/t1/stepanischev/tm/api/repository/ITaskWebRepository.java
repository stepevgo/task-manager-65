package ru.t1.stepanischev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.stepanischev.tm.model.TaskDTO;

@Repository
public interface ITaskWebRepository extends JpaRepository<TaskDTO, String> {
}