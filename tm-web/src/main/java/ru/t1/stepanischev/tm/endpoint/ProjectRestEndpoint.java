package ru.t1.stepanischev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.stepanischev.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.stepanischev.tm.model.ProjectDTO;
import ru.t1.stepanischev.tm.service.ProjectService;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public @Nullable Collection<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/findById/{id}")
    public @Nullable ProjectDTO findById(@PathVariable("id") String id) {
        return projectService.findOneById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody final ProjectDTO project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(@PathVariable("id") String id) {
        projectService.removeById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public ProjectDTO save(@RequestBody final ProjectDTO project) {
        projectService.add(project);
        return project;
    }

}