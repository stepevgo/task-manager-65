package ru.t1.stepanischev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanischev.tm.api.repository.IProjectWebRepository;
import ru.t1.stepanischev.tm.model.ProjectDTO;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectWebRepository projectRepository;

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Transactional
    public void update(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Nullable
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        projectRepository.deleteById(id);
    }

    @Nullable
    public ProjectDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return projectRepository.findById(id).orElse(null);
    }

}