package ru.t1.stepanischev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.stepanischev.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.stepanischev.tm.model.TaskDTO;
import ru.t1.stepanischev.tm.service.TaskService;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody TaskDTO task) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(@PathVariable("id") String id) {
        taskService.removeById(id);
    }

    @Nullable
    @Override
    @GetMapping("/findAll")
    public Collection<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@PathVariable("id") String id) {
        return taskService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public TaskDTO save(@RequestBody TaskDTO task) {
        taskService.add(task);
        return task;
    }

}
